import numpy as np
from scipy.optimize import minimize
import scipy.linalg as sl
from abc import ABC, abstractmethod

class Problem:
    def __init__(self, objFunc, dimensions, gradient=None, ):
        self.objFunc = objFunc
        self.dimensions = dimensions
        self.gradient = gradient
        if not gradient:
            self.gradient = self.computeGradient()

    def computeGradient(self):
        grad = []
        I = np.identity(self.dimensions)
        h = lambda x: x * 10 ** -8 if x != 0 else 10 ** -8
        grad.append(lambda x: np.array([(self.objFunc(x + I[:, 0] * h(x[0])) - self.objFunc(x)) / h(x[0])]))
        for i in range(self.dimensions - 1):
            k = i + 1
            grad.append(
                lambda x: np.append(grad[k - 1](x), (self.objFunc(x + I[:, k] * h(x[k])) - self.objFunc(x)) / h(x[k])))
        return grad[self.dimensions - 1]

class NewtonMethod:
    def __init__(self, problem, x0):
        self.problem = problem
        self.x0 = x0

    def solve(self, exact=False, limit=10000, printSteps=False):
        self.exact = exact
        xk, Qk = self.getInitail()
        steps = [np.copy(xk)]
        try:
            k = 0
            while not self.quitCondition(xk):
                if printSteps:
                    print(k, ':', xk)
                if k >= limit:
                    raise RuntimeError("did not converge after " + str(k) + " iterations")
                xk, Qk = self.step(xk, Qk)
                steps.append(np.copy(xk))
                k += 1
            return xk, steps
        except RuntimeError as e:
            print('No solution found,', e)
            return None, steps

    def getInitail(self):
        gra = self.problem.gradient
        n = self.problem.dimensions
        x0 = np.copy(self.x0)
        G = self.hessian(gra, x0, n)
        Q0 = np.linalg.inv(G)
        return x0, Q0

    def quitCondition(self, xk, epsilon=1e-8):
        gra = self.problem.gradient
        return np.all(abs(gra(xk)) < epsilon)

    def step(self, xk, Qk):
        gra = self.problem.gradient
        Gk = self.hessian(gra, xk, len(xk))
        Qk = np.linalg.inv(Gk)
        sk = -Qk @ gra(xk)
        try:
            if self.exact:
                alpha = self.exactLineSearch(xk, sk)
                xk += alpha * sk
            else:
                alpha = self.inexactLineSearch(xk, sk)
                xk += alpha * sk
        except FloatingPointError as e:
            raise RuntimeError("Numeric error in linesearch")
        return xk, Qk

    def hessian(self, gradient, x, n, H=1e-6):
        G = np.zeros([n, n])
        for j in range(n):
            ej = np.zeros(np.shape(x))
            ej[j] = 1
            h = H * x[j]
            if h == 0:
                h = H
            G1 = (gradient(x + h * ej) - gradient(x)) / (h)
            G[:, j] = G1
        G = (G + np.transpose(G)) / 2
        return G

    def exactLineSearch(self, xk, sk):
        fbar = 0  # TODO how to choose lower bound on function?
        rho = 0.1
        funcval = self.problem.objFunc(xk)
        mu = (-fbar + funcval) / (rho * np.linalg.norm(self.problem.gradient(xk)))
        alpha0 = mu / 10
        fAlpha = lambda alpha0: self.problem.objFunc(xk + alpha0 * sk)
        alpha0 = np.array([1])
        alphaopt = minimize(fAlpha, alpha0).x[0]
        return alphaopt

    def inexactLineSearch(self, xk, sk):
        fAlpha = lambda a: self.problem.objFunc(xk + a * sk)
        fPrimAlpha = lambda a: np.dot(self.problem.gradient(xk + a * sk), sk)
        rho = 0.1
        # sigma = 0.7 # used in a different variation of LC and RC
        tau = 0.1
        chi = 9.0
        alpha0 = 1
        alphaL = 0
        alphaU = 10e99
        LC = fAlpha(alpha0) >= fAlpha(alphaL) + \
             (1 - rho) * (alpha0 - alphaL) * fPrimAlpha(alphaL)
        RC = fPrimAlpha(alpha0) <= fAlpha(alphaL) + \
             rho * (alpha0 - alphaL) * fPrimAlpha(alphaL)
        while not (LC and RC):
            # print(alpha0)
            # print(alphaL)
            # print(alphaU)
            if alphaU == alphaL:
                break
            if not LC:
                if np.isclose(alphaL, alpha0):
                    deltaAlpha0 = 0
                else:
                    deltaAlpha0 = (alpha0 - alphaL) * fPrimAlpha(alpha0) / (fPrimAlpha(alphaL) - fPrimAlpha(alpha0))
                deltaAlpha0 = max(deltaAlpha0, tau * (alpha0 - alphaL))
                deltaAlpha0 = min(deltaAlpha0, chi * (alpha0 - alphaL))
                alphaL = alpha0
                alpha0 = alpha0 + deltaAlpha0
            else:
                alphaU = min(alpha0, alphaU)
                if alphaU == alphaL:
                    break
                alphaBar0 = (((alpha0 - alphaL) ** 2) * fPrimAlpha(alphaL)) / (
                            2 * (fAlpha(alphaL) - fAlpha(alpha0) + (alpha0 - alphaL) * fPrimAlpha(alphaL)))
                alphaBar0 = max(alphaBar0, alphaL + tau * (alphaU - alphaL))
                alphaBar0 = min(alphaBar0, alphaU - tau * (alphaU - alphaL))
                alpha0 = alphaBar0

            LC = fAlpha(alpha0) >= fAlpha(alphaL) + \
                 (1 - rho) * (alpha0 - alphaL) * fPrimAlpha(alphaL)
            RC = fPrimAlpha(alpha0) <= fAlpha(alphaL) + \
                 rho * (alpha0 - alphaL) * fPrimAlpha(alphaL)
        return alpha0

class QuasiNewton(NewtonMethod, ABC):
    def getInitial(self):
        Q0 = np.eye(self.problem.dimensions, self.problem.dimensions)
        x0 = np.copy(self.x0)
        return x0, Q0

    def step(self, xk, Qk):
        #update x
        try:
            sk = -Qk @ self.problem.gradient(xk)
            if self.exact:
                ak = self.exactLineSearch(xk, sk)
            else:
                ak = self.inexactLineSearch(xk, sk)
            xkp1 = xk + ak * sk
        except FloatingPointError:
            raise RuntimeError("Numeric error in linesearch")
        if self.quitCondition(xkp1):
            return xkp1, None
        else:
            deltax = xkp1 - xk
            deltag = self.problem.gradient(xkp1) - self.problem.gradient(xk)
            try:
                Qkp1 = self.updateQ(Qk, deltax, deltag)
            except FloatingPointError:
                raise RuntimeError("Numeric error in Hessian approximation")
            return xkp1, Qkp1
    
    @abstractmethod
    def updateQ(self, Qk, deltax, deltag):
        pass


class Broyden(QuasiNewton):
    def updateQ(self, Qk, deltax, deltag):
        Qkp1 = Qk + np.outer(((deltax - Qk @ deltag) / (deltax @ Qk @ deltag)), deltax) * Qk
        return Qkp1


class BroydenBad(QuasiNewton):
    def updateQ(self, Qk, deltax, deltag):
        Qkp1 = Qk + np.outer(((deltag - Qk @ deltax) / (deltax @ deltax)), deltax)
        return Qkp1


class DFP(QuasiNewton):
    def updateQ(self, Qk, deltax, deltag):
        Qkp1 = (Qk + (np.outer(deltax, deltax)) / (np.transpose(deltax) @ deltag)
                - (((Qk) @ np.outer(deltag, deltag)) @ Qk) / (np.transpose(deltag) @ Qk @ deltag))
        return Qkp1


class BFGS(QuasiNewton):
    def updateQ(self, Qk, deltax, deltag):
        Qkp1 = (Qk +
                (((np.transpose(deltax) @ deltag + np.transpose(deltag) @ (Qk @ deltag)) * (np.outer(deltax, deltax)))
                 / ((np.transpose(deltax) @ deltag) ** 2))
                -
                (((Qk @ (np.outer(deltag, deltax)) +
                   (np.outer(deltax, deltag)) @ Qk)) /
                 (np.transpose(deltax) @ deltag)))
        return Qkp1


