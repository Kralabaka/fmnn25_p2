import numpy as np
from scipy.optimize import minimize
import scipy.linalg as sl
import matplotlib
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy.linalg as nl


class NewtonMethod:
    def __init__(self, problem):
        self.problem = problem

    def solve(self, exact=None):
        gra = self.problem.gradient
        n = self.problem.dimensions
        xk = np.array([-0.3, -0.8])  # initial guess
        steps = [np.copy(xk)]
        k = 0
        while not np.allclose(gra(xk), [0] * n):
            k += 1
            Gk = self.hessian(gra, xk, n)
            if exact is None: #exact == None
                try:
                    cho = sl.cho_factor(Gk)
                    alpha = 1
                    x = sl.cho_solve(cho, alpha * gra(xk))
                    xk += - x
                except sl.LinAlgError:
                    raise sl.LinAlgError('The Hessian of the objective function is not positive definite in', xk)
            if exact: #extact == true
                sk = -np.linalg.inv(Gk) @ gra(xk)
                alpha = self.exactLineSearch(xk, sk)
                xk += alpha * sk
            else: #exact == false
                sk = -np.linalg.inv(Gk) @ gra(xk)
                alpha = self.inexactLineSearch(xk, sk)
                xk += alpha * sk
            steps.append(np.copy(xk))
            if k > 100000:
                print('did not converge')
                break
        return steps[-1]

    def hessian(self, gradient, x, n, H=1e-6):
        G = np.zeros([n, n])
        for j in range(n):
            ej = np.zeros(np.shape(x))
            ej[j] = 1
            h = H * x[j]
            if h == 0:
                h = H
            G1 = (gradient(x + h * ej) - gradient(x)) / (h)
            h = x[j] * H
            G1 = (self.problem.gradient(x + h * ej) - self.problem.gradient(x)) / (h)
            G[:, j] = G1
        G = (G + np.transpose(G)) / 2
        return G

    def exactLineSearch(self, xk, sk):
        fbar = 0  # TODO how to choose lower bound on function?
        rho = 0.1
        funcval = self.problem.objFunc(xk)
        mu = (-fbar + funcval) / (rho * np.linalg.norm(self.problem.gradient(xk)))
        alpha0 = mu / 10
        fAlpha = lambda alpha0: self.problem.objFunc(xk + alpha0 * sk)
        alphaopt = minimize(fAlpha, alpha0).x[0]
        return alphaopt

    def inexactLineSearch(self, xk, sk):
        fAlpha = lambda a: self.problem.objFunc(xk + a * sk)
        fPrimAlpha = lambda a: np.dot(self.problem.gradient(xk + a * sk), sk)
        rho = 0.1
        # sigma = 0.7 # used in a different variation of LC and RC
        tau = 0.1
        chi = 9.0
        alpha0 = 1
        alphaL = 0
        alphaU = 10e99
        LC = False
        RC = False
        while not (LC and RC):
            # print(alpha0)
            # print(alphaL)
            # print(alphaU)
            if alphaU == alphaL:
                break
            if not LC:
                if np.isclose(alphaL, alpha0):
                    deltaAlpha0 = 0
                else:
                    deltaAlpha0 = (alpha0 - alphaL) * fPrimAlpha(alpha0) / (fPrimAlpha(alphaL) - fPrimAlpha(alpha0))
                deltaAlpha0 = max(deltaAlpha0, tau * (alpha0 - alphaL))
                deltaAlpha0 = min(deltaAlpha0, chi * (alpha0 - alphaL))
                alphaL = alpha0
                alpha0 = alpha0 + deltaAlpha0
            else:
                alphaU = min(alpha0, alphaU)
                if alphaU == alphaL:
                    break
                alphaBar0 = (((alpha0 - alphaL) ** 2) * fPrimAlpha(alphaL)) / (
                            2 * (fAlpha(alphaL) - fAlpha(alpha0) + (alpha0 - alphaL) * fPrimAlpha(alphaL)))
                alphaBar0 = max(alphaBar0, alphaL + tau * (alphaU - alphaL))
                alphaBar0 = min(alphaBar0, alphaU - tau * (alphaU - alphaL))
                alpha0 = alphaBar0

            LC = fAlpha(alpha0) >= fAlpha(alphaL) + \
                 (1 - rho) * (alpha0 - alphaL) * fPrimAlpha(alphaL)
            RC = fPrimAlpha(alpha0) <= fAlpha(alphaL) + \
                 rho * (alpha0 - alphaL) * fPrimAlpha(alphaL)
        return alpha0


class Problem:

    def __init__(self, objFunc, dimensions, gradient=None, ):
        self.objFunc = objFunc
        self.dimensions = dimensions
        self.gradient = gradient
        if not gradient:
            self.gradient = self.computeGradient()

    def computeGradient(self):
        grad = []
        I = np.identity(self.dimensions)
        h = lambda x: x * 10 ** -8 if x != 0 else 10 ** -8
        grad.append(lambda x: np.array([(self.objFunc(x + I[:, 0] * h(x[0])) - self.objFunc(x)) / h(x[0])]))
        for i in range(self.dimensions - 1):
            k = i + 1
            grad.append(
                lambda x: np.append(grad[k - 1](x), (self.objFunc(x + I[:, k] * h(x[k])) - self.objFunc(x)) / h(x[k])))
        return grad[self.dimensions - 1]


class Broyden(NewtonMethod):
    def __init__(self, problem, x):
        super().__init__(problem)
        self.gradient = problem.gradient
        H0 = self.hessian(self.gradient, x, problem.dimensions)
        self.Q0 = nl.inv(H0)
        self.x = x

    def Qstep(self, Qinv, x0, x1, epsilon=1e-16):
        deltax = x1 - x0
        deltaf = self.gradient(x1) - self.gradient(x0)
        deltaobjfunc = self.problem.objFunc(x1) - self.problem.objFunc(x0)
        if (abs(deltaobjfunc) < epsilon):
            return (False)
        #print(deltaobjfunc)
        Qinv = Qinv + ((deltax - Qinv @ deltaf) / (np.transpose(deltax) @ Qinv @ deltaf)) @ np.transpose(deltax) * Qinv
        return (Qinv)

    def solve(self, exactlinesearch=False):
        Q0 = self.Q0
        x0 = self.x
        while np.any(Q0):
            sk = -Q0 @ self.gradient(x0)
            if exactlinesearch:
                ak = self.exactLineSearch(x0, sk)
            else:
                ak = self.inexactLineSearch(x0, sk)
            x1 = x0 + ak * sk
            Q0 = self.Qstep(Q0, x0, x1)
            x0 = x1
        return (x0)

    def badstep(self, Qinv, x0, x1, epsilon=1e-10):
        deltax = x1 - x0
        deltaf = self.gradient(x1) - self.gradient(x0)
        deltaobjfunc = self.problem.objFunc(x1) - self.problem.objFunc(x0)
        if all(abs(deltax) < epsilon) or all(abs(deltaf) < epsilon):
            return (False)
        Qinv = Qinv + ((deltax - Qinv @ deltaf) / (np.transpose(deltaf) @ deltaf)) @ np.transpose(deltax)
        return (Qinv)

    def badsolve(self, exactlinesearch=False):
        Q0 = self.Q0
        x0 = self.x
        while np.any(Q0):
            sk = -Q0 @ self.gradient(x0)
            if exactlinesearch:
                ak = self.exactLineSearch(x0, sk)
            else:
                ak = self.inexactLineSearch(x0, sk)
            x1 = x0 + ak * sk
            Q0 = self.badstep(Q0, x0, x1)
            x0 = x1
        return (x0)


class DFP(NewtonMethod):
    def __init__(self, problem, x):
        super().__init__(problem)
        self.gradient = problem.gradient
        H0 = self.hessian(self.gradient, x, problem.dimensions)
        self.Q0 = nl.inv(H0)
        self.x = x

    def step(self, Qinv, x0, x1, epsilon=1e-14):
        deltaf = self.gradient(x1) - self.gradient(x0)
        deltax = x1 - x0
        deltaobjfunc = self.problem.objFunc(x1) - self.problem.objFunc(x0)
        if (abs(deltaobjfunc) < epsilon):
            return (False)
        Qinv = (Qinv + (deltax @ np.transpose(deltax)) / (np.transpose(deltax) @ deltaf)
                - ((Qinv @ deltaf) @ (np.transpose(deltaf) @ Qinv)) / (np.transpose(deltaf) @ Qinv @ deltaf))
        return (Qinv)

    def solve(self, exactlinesearch=False):
        Q0 = self.Q0
        x0 = self.x
        while np.any(Q0):
            sk = -Q0 @ self.gradient(x0)
            if exactlinesearch:
                ak = self.exactLineSearch(x0, sk)
            else:
                ak = self.inexactLineSearch(x0, sk)
            x1 = x0 + ak * sk
            #print(x1)
            Q0 = self.step(Q0, x0, x1)
            x0 = x1
        return (x0)


class BFGS(NewtonMethod):
    def __init__(self, problem, x):
        super().__init__(problem)
        self.gradient = problem.gradient
        H0 = self.hessian(self.gradient, x, problem.dimensions)
        self.Q0 = nl.inv(H0)
        self.x = x

    def step(self, Qinv, x0, x1, epsilon=1e-16):
        yk = self.gradient(x1) - self.gradient(x0)
        sk = x1 - x0
        deltaobjfunc = self.problem.objFunc(x1) - self.problem.objFunc(x0)
        if (abs(deltaobjfunc) < epsilon):
            return (False)
        isk = sk @ np.transpose(sk)
        Qinv = (Qinv +
                (((np.transpose(sk) @ yk + np.transpose(yk) @ (Qinv @ yk)) * (sk @ np.transpose(sk)))
                 / ((np.transpose(sk) @ yk) ** 2))
                -
                (((Qinv @ yk)
                  @ np.transpose(sk)) +
                 sk @ (np.transpose(yk) @ Qinv)) /
                (np.transpose(sk) @ yk))
        return (Qinv)

    def solve(self, exactlinesearch=False):
        Q0 = self.Q0
        x0 = self.x
        while np.any(Q0):
            sk = -Q0 @ self.gradient(x0)
            if exactlinesearch:
                ak = self.exactLineSearch(x0, sk)
            else:
                ak = self.inexactLineSearch(x0, sk)
            x1 = x0 + ak * sk
            #print(x1)
            Q0 = self.step(Q0, x0, x1)
            x0 = x1
        return (x0)

