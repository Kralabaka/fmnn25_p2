import numpy as np
from scipy.optimize import minimize
import scipy.linalg as sl
import matplotlib
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy.linalg as nl
from  scipy import dot,linspace
import scipy.optimize as so
from numpy import array


class NewtonMethod:
    def __init__(self, problem):
        self.problem = problem
        pass

    def solve(self, exact=None):
        gra = self.problem.gradient
        n = self.problem.dimensions
        xk = np.array([-0.3, -0.8])  # initial guess
        steps = [np.copy(xk)]
        k = 0
        while not np.allclose(gra(xk), [0] * n):
            k += 1
            """
            Gk = self.hessian(gra, xk, n)
            cho = sl.cho_factor(Gk)
            alpha = 1
            x = sl.cho_solve(cho, alpha * gra(xk))
            xk = xk - x

            """
            Gk = self.hessian(gra, xk, n)
            if exact == None:
                try:
                    cho = sl.cho_factor(Gk)
                    alpha = 1
                    x = sl.cho_solve(cho, alpha * gra(xk))
                    xk += - x
                except sl.LinAlgError:
                    raise sl.LinAlgError('The Hessian of the objective function is not positive definite in', xk)
            if exact == True:
                sk = -np.linalg.inv(Gk) @ gra(xk)
                alpha = self.exactLineSearch(xk, sk)
                xk += alpha * sk
            elif exact == False:
                sk = -np.linalg.inv(Gk) @ gra(xk)
                alpha = self.inexactLineSearch(xk, sk)
                xk += alpha * sk
            steps.append(np.copy(xk))
            if k > 1000:
                print('did not converge')
                break
        return steps

    def hessian(self, gradient, x, n, H=1e-6):
        G = np.zeros([n, n])
        for j in range(n):
            ej = np.zeros(np.shape(x))
            ej[j] = 1
            h = H * x[j]
            if h == 0:
                h = H
            G1 = (gradient(x + h * ej) - gradient(x)) / (h)
            h = x[j] * H
            G1 = (self.problem.gradient(x + h * ej) - self.problem.gradient(x)) / (h)
            G[:, j] = G1
        G = (G + np.transpose(G)) / 2
        return G

    def exactLineSearch(self, xk, sk):
        fbar = 0  # TODO how to choose lower bound on function?
        rho = 0.1
        funcval = self.problem.objFunc(xk)
        mu = (-fbar + funcval) / (rho * np.linalg.norm(self.problem.gradient(xk)))
        alpha0 = mu / 10
        fAlpha = lambda alpha0: self.problem.objFunc(xk + alpha0 * sk)
        alphaopt = minimize(fAlpha, alpha0).x[0]
        return alphaopt

    def inexactLineSearch(self, xk, sk):
        fAlpha = lambda a: self.problem.objFunc(xk + a * sk)
        fPrimAlpha = lambda a: np.dot(self.problem.gradient(xk + a * sk), sk)
        rho = 0.1
        # sigma = 0.7 # used in a different variation of LC and RC
        tau = 0.1
        chi = 9.0
        alpha0 = 1
        alphaL = 0
        alphaU = 10e99
        LC = fAlpha(alpha0) >= fAlpha(alphaL) + \
             (1 - rho) * (alpha0 - alphaL) * fPrimAlpha(alphaL)
        RC = fPrimAlpha(alpha0) <= fAlpha(alphaL) + \
             rho * (alpha0 - alphaL) * fPrimAlpha(alphaL)
        while not (LC and RC):
            #print(alpha0)
            #print(alphaL)
            #print(alphaU)
            if alphaU == alphaL:
                break
            if not LC:
                if np.isclose(alphaL, alpha0):
                    deltaAlpha0 = 0
                else:
                    deltaAlpha0 = (alpha0 - alphaL) * fPrimAlpha(alpha0) / (fPrimAlpha(alphaL) - fPrimAlpha(alpha0))
                deltaAlpha0 = max(deltaAlpha0, tau * (alpha0 - alphaL))
                deltaAlpha0 = min(deltaAlpha0, chi * (alpha0 - alphaL))
                alphaL = alpha0
                alpha0 = alpha0 + deltaAlpha0
            else:
                alphaU = min(alpha0, alphaU)
                if alphaU == alphaL:
                    break
                alphaBar0 = (((alpha0 - alphaL) ** 2) * fPrimAlpha(alphaL)) / (
                            2 * (fAlpha(alphaL) - fAlpha(alpha0) + (alpha0 - alphaL) * fPrimAlpha(alphaL)))
                alphaBar0 = max(alphaBar0, alphaL + tau * (alphaU - alphaL))
                alphaBar0 = min(alphaBar0, alphaU - tau * (alphaU - alphaL))
                alpha0 = alphaBar0

            LC = fAlpha(alpha0) >= fAlpha(alphaL) + \
                 (1 - rho) * (alpha0 - alphaL) * fPrimAlpha(alphaL)
            RC = fPrimAlpha(alpha0) <= fAlpha(alphaL) + \
                 rho * (alpha0 - alphaL) * fPrimAlpha(alphaL)
        return alpha0


class Problem:

    def __init__(self, objFunc, dimensions, gradient=None, ):
        self.objFunc = objFunc
        self.dimensions = dimensions
        self.gradient = gradient
        if not gradient:
            self.gradient = self.computeGradient()

    def computeGradient(self):
        grad = []
        I = np.identity(self.dimensions)
        h = lambda x: x * 10 ** -8 if x != 0 else 10 ** -8
        grad.append(lambda x: np.array([(self.objFunc(x + I[:, 0] * h(x[0])) - self.objFunc(x)) / h(x[0])]))
        for i in range(self.dimensions - 1):
            k = i + 1
            grad.append(
                lambda x: np.append(grad[k - 1](x), (self.objFunc(x + I[:, k] * h(x[k])) - self.objFunc(x)) / h(x[k])))
        return grad[self.dimensions - 1]


def rosenbrock(x):
    # return 100*(x[1] - x[0]**2)**2 + (1 - x[0])**2
    return x[0] ** 2 + x[1] ** 2


class Broyden(NewtonMethod):
    def __init__(self, problem, x):
        self.problem = problem
        self.gradient = problem.computeGradient()
        H0 = self.hessian(self.gradient, x, problem.dimensions)
        self.Q0 = nl.inv(H0)
        self.x = x

    def Qstep(self, Qinv, x0, x1, epsilon=1e-16):
        deltax = x1 - x0
        deltaf = self.gradient(x1) - self.gradient(x0)
        deltaobjfunc = self.problem.objFunc(x1) - self.problem.objFunc(x0)
        if all((abs(self.gradient(x1))) < epsilon):
            return (False)
        Qinv = Qinv + ((deltax - Qinv @ deltaf) / (np.transpose(deltax) @ Qinv @ deltaf)) @ np.transpose(deltax) * Qinv
        return (Qinv)

    def solve(self, exactlinesearch=False):
        Q0 = self.Q0
        x0 = self.x
        while np.any(Q0):
            sk = -Q0 @ self.gradient(x0)
            if exactlinesearch:
                ak = self.exactLineSearch(x0, sk)
            else:
                ak = self.inexactLineSearch(x0, sk)
            x1 = x0 + ak * sk
            Q0 = self.Qstep(Q0, x0, x1)
            x0 = x1
        return (x0)

    def badstep(self, Qinv, x0, x1, epsilon=1e-10):
        deltax = x1 - x0
        deltaf = self.gradient(x1) - self.gradient(x0)
        deltaobjfunc = self.problem.objFunc(x1) - self.problem.objFunc(x0)
        if all((abs(self.gradient(x1))) < epsilon):
            return (False)
        Qinv = Qinv + ((deltax - Qinv @ deltaf) / (np.transpose(deltaf) @ deltaf)) @ np.transpose(deltax)
        return (Qinv)

    def badsolve(self, exactlinesearch=False):
        Q0 = self.Q0
        x0 = self.x
        while np.any(Q0):
            sk = -Q0 @ self.gradient(x0)
            if exactlinesearch:
                ak = self.exactLineSearch(x0, sk)
            else:
                ak = self.inexactLineSearch(x0, sk)
            x1 = x0 + ak * sk
            Q0 = self.badstep(Q0, x0, x1)
            x0 = x1
        return (x0)


class DFP(NewtonMethod):
    def __init__(self, problem, x):
        self.problem = problem
        self.gradient = problem.gradient
        self.x = x

    def step(self, Qinv, x0, x1, epsilon=1e-14):
        deltaf = self.gradient(x1) - self.gradient(x0)
        deltax = x1 - x0
        deltaobjfunc = self.problem.objFunc(x1) - self.problem.objFunc(x0)
        if all((abs(self.gradient(x1))) < epsilon):
            return (False)
        Qinv = (Qinv + (np.outer(deltax , deltax)) / (np.transpose(deltax) @ deltaf)
                - (((Qinv) @ np.outer(deltaf,deltaf)) @ Qinv) / (np.transpose(deltaf) @ Qinv @ deltaf))
        return (Qinv)

    def solve(self, exactlinesearch=False):
        Q0 = np.eye(self.problem.dimensions,self.problem.dimensions)
        x0 = self.x
        while np.any(Q0):
            sk = -Q0 @ self.gradient(x0)
            if exactlinesearch:
                ak = self.exactLineSearch(x0, sk)
            else:
                ak = self.inexactLineSearch(x0, sk)
            x1 = x0 + ak * sk
            Q0 = self.step(Q0, x0, x1)
            x0 = x1
        return (x0)


class BFGS(NewtonMethod):
    def __init__(self, problem, x):
        self.problem = problem
        self.gradient = problem.gradient
        self.x = x

    def step(self, Qinv, x0, x1, epsilon=1e-14):
        yk = self.gradient(x1) - self.gradient(x0)
        sk = x1 - x0
        deltaobjfunc = self.problem.objFunc(x1) - self.problem.objFunc(x0)
        if (sl.norm(self.gradient(x0))) < epsilon:
            return (1)
        isk = sk @ np.transpose(sk)
        Qinv = (Qinv +
                (((np.transpose(sk) @ yk + np.transpose(yk) @ (Qinv @ yk)) * (np.outer(sk,sk)))
                 / ((np.transpose(sk) @ yk) ** 2))
                -
                (((Qinv @ (np.outer(yk,sk))+
                (np.outer(sk ,yk)) @ Qinv)) /
                (np.transpose(sk) @ yk)))
        return (Qinv)

    def solve(self, exactlinesearch=False):
        Q0 = np.eye(self.problem.dimensions,self.problem.dimensions)
        x0 = self.x
        while Q0 is not 1:
            pk = -Q0 @ self.gradient(x0)
            if exactlinesearch:
                ak = self.exactLineSearch(x0, pk)
            else:
                ak = self.inexactLineSearch(x0, pk)
            sk=ak*pk
            x1=x0+sk
            Q0 = self.step(Q0, x0, x1)
            x0 = x1
        return (x0)





def T(x, n):
    """
    Recursive evaluation of the Chebychev Polynomials of the first kind
    x evaluation point (scalar)
    n degree
    """
    if n == 0:
        return 1.0
    if n == 1:
        return x
    return 2. * x * T(x, n - 1) - T(x, n - 2)


def U(x, n):
    """
    Recursive evaluation of the Chebychev Polynomials of the second kind
    x evaluation point (scalar)
    n degree
    Note d/dx T(x,n)= n*U(x,n-1)
    """
    if n == 0:
        return 1.0
    if n == 1:
        return 2. * x
    return 2. * x * U(x, n - 1) - U(x, n - 2)


def chebyquad_fcn(x):
    """
    Nonlinear function: R^n -> R^n
    """
    n = len(x)

    def exact_integral(n):
        """
        Generator object to compute the exact integral of
        the transformed Chebychev function T(2x-1,i), i=0...n
        """
        for i in range(n):
            if i % 2 == 0:
                yield -1. / (i ** 2 - 1.)
            else:
                yield 0.

    exint = exact_integral(n)

    def approx_integral(i):
        """
        Approximates the integral by taking the mean value
        of n sample points
        """
        return sum(T(2. * xj - 1., i) for xj in x) / n

    return array([approx_integral(i) - e for i, e in enumerate(exint)])


def chebyquad(x):
    """
    norm(chebyquad_fcn)**2
    """
    chq = chebyquad_fcn(x)
    return dot(chq, chq)


def gradchebyquad(x):
    """
    Evaluation of the gradient function of chebyquad
    """
    chq = chebyquad_fcn(x)
    UM = 4. / len(x) * array([[(i + 1) * U(2. * xj - 1., i)
                               for xj in x] for i in range(len(x) - 1)])
    return dot(chq[1:].reshape((1, -1)), UM).reshape((-1,))

n=11

if __name__ == '__main__':
    x = linspace(0, 1, n)
    xmin = so.fmin_bfgs(chebyquad, x, gradchebyquad)  # should converge after 18 iterations

rosenbrock = lambda x: 100*(x[1] - x[0]**2)**2 + (1 - x[0])**2
cheb=lambda x:chebyquad(x)
chebgrad=lambda x:gradchebyquad(x)

P=Problem(cheb,n,chebgrad)
S=BFGS(P,x)
xmini=S.solve(False)

print('our',xmini)
print('python',xmin)