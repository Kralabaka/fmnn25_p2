class Broyden(NewtonMethod):
    def __init__(self,problem,x):
        self.problem=problem
        self.gradient=problem.computeGradient()
        H0=self.hessian(self.gradient,x,problem.dimensions)
        self.Q0=nl.inv(H0)
        self.x=x
    def Qstep(self,Qinv,x0,x1,epsilon=1e-16):
        deltax=x1-x0
        deltaf=self.gradient(x1)-self.gradient(x0)
        if all(abs(deltax)<epsilon) or all(abs(deltaf)<epsilon):
            return(False)
        Qinv=Qinv+((deltax-Qinv@deltaf)/(np.transpose(deltax)@Qinv@deltaf))@np.transpose(deltax)*Qinv
        return(Qinv)
    def Broydensolve(self,exactlinesearch=False):
        Q0=self.Q0
        x0=self.x
        while np.any(Q0):
            sk=-Q0@self.gradient(x0)
            if exactlinesearch:
                ak=self.exactLineSearch(x0,sk)
            else:
                ak=self.inexactLineSearch(x0,sk)
            x1=x0+ak*sk
            Q0=self.Qstep(Q0,x0,x1)
            x0=x1
        return(x0)
    def badstep(self,Qinv,x0,x1,epsilon=1e-10):
        deltax=x1-x0
        deltaf=self.gradient(x1)-self.gradient(x0)
        if all(abs(deltax)<epsilon) or all(abs(deltaf)<epsilon):
            return(False)
        Qinv=Qinv+((deltax-Qinv@deltaf)/(np.transpose(deltaf)@deltaf))@np.transpose(deltax)
        return(Qinv)
    def badsolve(self,exactlinesearch=False):
        Q0=self.Q0
        x0=self.x
        while np.any(Q0):
            sk=-Q0@self.gradient(x0)
            if exactlinesearch:
                ak=self.exactLineSearch(x0,sk)
            else:
                ak=self.inexactLineSearch(x0,sk)
            x1=x0+ak*sk
            Q0=self.badstep(Q0,x0,x1)
            x0=x1
        return(x0)
        
class DFP(NewtonMethod):
    def __init__(self,problem,x):
        self.problem=problem
        self.gradient=problem.computeGradient()
        H0=self.hessian(self.gradient,x,problem.dimensions)
        self.Q0=nl.inv(H0)
        self.x=x
    def step(self,Qinv,x0,x1,epsilon=1e-12):
        deltaf=self.gradient(x1)-self.gradient(x0)
        deltax=x1-x0
        if all(abs(deltax)<epsilon) or all(abs(deltaf)<epsilon):
            return(False)
        Qinv=(Qinv+(deltax@np.transpose(deltax))/(np.transpose(deltax)@deltaf)
        -((Qinv@deltaf)@(np.transpose(deltaf)@Qinv))/(np.transpose(deltaf)@Qinv@deltaf))
        return(Qinv)
    def solve(self,exactlinesearch=False):
        Q0=self.Q0
        x0=self.x
        while np.any(Q0):
            sk=-Q0@self.gradient(x0)
            if exactlinesearch:
                ak=self.exactLineSearch(x0,sk)
            else:
                ak=self.inexactLineSearch(x0,sk)
            x1=x0+ak*sk
            print(x1)
            Q0=self.step(Q0,x0,x1)
            x0=x1
        return(x0)
        
class BFGS(NewtonMethod):
    def __init__(self,problem,x):
        self.problem=problem
        self.gradient=problem.computeGradient()
        H0=self.hessian(self.gradient,x,problem.dimensions)
        self.Q0=nl.inv(H0)
        self.x=x
    def step(self,Qinv,x0,x1,epsilon=1e-12):
        deltaf=self.gradient(x1)-self.gradient(x0)
        deltax=x1-x0
        I=np.eye(self.problem.dimensions)
        if all(abs(deltax)<epsilon) or all(abs(deltaf)<epsilon):
            return(False)
        Qinv=((I-(deltax@np.transpose(deltaf))/(np.transpose(deltaf)@deltax))@Qinv@
        (I-(deltaf@np.transpose(deltax))/(np.transpose(deltaf)@deltax))+
        (deltax@np.transpose(deltax))/(np.transpose(deltaf)@deltax))
        return(Qinv)
    def solve(self,exactlinesearch=False):
        Q0=self.Q0
        x0=self.x
        while np.any(Q0):
            sk=-Q0@self.gradient(x0)
            if exactlinesearch:
                ak=self.exactLineSearch(x0,sk)
            else:
                ak=self.inexactLineSearch(x0,sk)
            x1=x0+ak*sk
            print(x1)
            Q0=self.step(Q0,x0,x1)
            x0=x1
        return(x0)