from methodRefac import *
import time
import chebyquad
from scipy.optimize import minimize

rosenbrock = lambda x: 100 * (x[1] - x[0] ** 2) ** 2 + (1 - x[0]) ** 2

testfunc1 = lambda x: x[0]**2 + x[1]**2 + 3 # f(x,y) = x² + y² + 3

cheby = chebyquad.chebyquad
chebygrad = chebyquad.gradchebyquad

#np.seterr(all='raise')

p1 = Problem(testfunc1, 2)
p2 = Problem(rosenbrock, 2)
p3 = Problem(cheby, 4, gradient=chebygrad)


def test_all_methods(problem, x0):

    met1 = NewtonMethod(problem, x0)
    met2 = Broyden(problem, x0)
    met3 = BroydenBad(problem, x0)
    met4 = DFP(problem, x0)
    met5 = BFGS(problem, x0)

    t0 = time.clock()
    s = minimize(problem.objFunc, x0).x
    t1 = time.clock()
    print('scipy.minmize: x_min =', s, 'f(x_min) =', problem.objFunc(s), '\t time:', np.round(t1 - t0, 4), 's\n')

    test_method(met1, problem, 'Newton')
    test_method(met2, problem, 'Broyden')
    #test_method(met3, problem, 'BroydenBad')
    test_method(met4, problem, 'DFP')
    test_method(met5, problem, 'BFGS')


def test_method(method, problem, name):
    try:
        t0 = time.clock()
        s, steps = method.solve(True, 1000)
        t1 = time.clock()
        print(name, 'exact linesearch: x_min =', s, 'f(x_min) =', problem.objFunc(s), '\t time:', np.round(t1 - t0, 4), 's', '\t steps: ', len(steps))
    except (ValueError, IndexError, FloatingPointError, RuntimeError) as e:
        print(name, 'exact linesearch failed:', e)
        #raise e
    try:
        t0 = time.clock()
        s, steps = method.solve(False, 10000)
        t1 = time.clock()
        print(name, 'inexact linesearch: x_min =', s, 'f(x_min) =', problem.objFunc(s), '\t time:', np.round(t1 - t0, 4), 's', '\t steps: ', len(steps))
    except (ValueError, IndexError, FloatingPointError, RuntimeError) as e:
        print(name, 'inexact linesearch failed:', e)
        #raise e

    print('')
x1 = np.array([-2.0, -2.0])
x2 = np.array([1.2, 0.7])
x3 = np.linspace(0,1, 4)

print("------Function 1: x**2 + y**2 + 3------")
test_all_methods(p1, x1)
print("------Function 2: rosenbrock-----------")
test_all_methods(p2, x2)
print("------Function 3: chebyquad------------")
test_all_methods(p3, x3)